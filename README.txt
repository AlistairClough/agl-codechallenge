Programming challenge
A json web service has been set up at the url: http://agl-developer-test.azurewebsites.net/people.json

You need to write some code to consume the json and output a list of all the cats in alphabetical order under a heading of the gender of their owner.

You can write it in any language you like. You can use any libraries/frameworks/SDKs you choose.

Example:
Male
    Angel
    Molly
    Tigger
Female
    Gizmo
    Jasper

Notes
Submissions will only be accepted via github or bitbucket
Use industry best practices
Use the code to showcase your skill.

TASKS
Retreive json
Output gender
    output pet.name if pet.type==Cat
    sort alphabetically

1) Retrieve JSON and print to screen
2) Filter by gender
3) Filter null and undefined values
4) Filter by type
5) Sort alphabetically
6) Efficiency
6) Style 
   

JSON STRUCTURE
name
gender
age    
pets
    name
    type

TEST DATA EXPECTED OUTCOME
Male
    Garfield
    Jim
    Max
    Tom
Female  
    Garfield
    Simba
    Tabby

IDEAS
1) create keys [ownerGender, petName]  
2) create list male[petNames], female[petNames]  //More efficient option

Notes
Some people may not have pets. Others may have lots.
May need to filter by other pets in the future
Gender may include values other than Male or Female. Best to filter by specific values.